const fs = require('fs')
const chalk = require('chalk')

const getNotes =  () => {
    "Your notes..."
}

const readNotes = (title) => {
    const readNotes = loadNotes()
    const note = readNotes.find((note) => note.title === title)

    if(note){
        console.log(chalk.green.bold.inverse(note.title))
        console.log(note.body)
       
    } else {
        console.log(chalk.red('No note found'))
    }
}

const addNote = (title, body) => {
    const notes = loadNotes()
    const duplicateNotes = notes.filter((note) =>
        note.title === title
    )

    debugger
    const duplicateNote = notes.find((note) =>
    note.title === title)

    if (!duplicateNote) {
        notes.push({
            title: title,
            body: body
        })
        saveNotes(notes)
        console.log('New Note added!')
    }else {
        console.log('Note title taken')
    }
    
    // if (duplicateNotes.length === 0) {
    //     notes.push({
    //         title: title,
    //         body: body
    //     })
    //     saveNotes(notes)
    //     console.log('New Note added!')
    // }else {
    //     console.log('Note title taken')
    // }
    
}

const saveNotes = (notes) => {
    const dataJSON = JSON.stringify(notes)
    fs.writeFileSync('notes.json', dataJSON)
}

const loadNotes =  () => {
    try {
        const dataBuffer = fs.readFileSync('notes.json')
        const dataString = dataBuffer.toString()
        return JSON.parse(dataString)
    }catch (e) {
        return []
    }
}

const listNotes = () => {
    const listNotes = loadNotes()
    listNotes.forEach((note) => {
        console.log(note.title)
    });
    // return listNotes.map(note => {
    //     console.log(note.title)
    //     console.log(note.body)
    // })
}


const removeNote = (title) => {
    const notes= loadNotes()
    const notesToKeep = notes.filter((note) =>
         note.title !== title
    )
    saveNotes(notesToKeep)
    const successMessage = title + ' has been removed!'
    const noNote = 'No note found!'
    if (notes.length > notesToKeep.length) {
        console.log(chalk.green.inverse.bold(successMessage))
    }else{
        console.log(chalk.red.inverse.bold(noNote))
    }
    // function checktitle(title) {
    //     return removeTitle = notes.filter(function(note){
    //             return note.title === title
    //         })
            
    //     }
    // const remove = notes.filter(checktitle)
    // saveNotes(remove)
    
}

module.exports = {
    getNotes: getNotes,
    addNote: addNote,
    removeNote: removeNote,
    listNotes: listNotes,
    readNotes: readNotes
}
