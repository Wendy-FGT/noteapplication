const validator = require('validator');
const chalkstuff = require('chalk')
const notes = require('./notes.js');
const yargs = require('yargs');

//add, remove, read, list in notes

//Create add command
yargs.command({
    command: 'add',
    describe: 'Add a new note',
    builder: {
        title:{
            describe:'Note title',
            demandOption: true,
            type: 'string'
        },
        body: {
            describe:"Body of the note",
            demandOption: true,
            type: "string"
        }
    },
    handler (argv) {
       notes.addNote(argv.title, argv.body)
    }
})

//Create remove command 
yargs.command({
    command: 'remove',
    describe: 'Remove note',
    handler (argv) {
        notes.removeNote(argv.title)
    }
})

//Create read command
yargs.command({
    command: 'read',
    describe: 'To enable note reading',
    builder: {
        title:{
            describe:'Note title',
            demandOption: true,
            type: 'string'
        }
    },
    handler(argv){
        notes.readNotes(argv.title)
    }
})

//Create list command
yargs.command({
    command: 'list',
    describe: 'To enable listing of notes',
    handler () {
        notes.listNotes()
    }
})
yargs.parse()