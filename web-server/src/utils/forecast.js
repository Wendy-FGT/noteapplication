const request = require('request')

// Goal: Create a reusable function for getting the forecast
//
// 1. Setup the "forecast" function in utils/forecast.js
// 2. Require the function in app.js and call it as shown below
// 3. The forecast function should have three potential calls to callback:
//    - Low level error, pass string for error
//    - Coordinate error, pass string for error
//    - Success, pass forecast string for data (same format as from before)

const forecast = (address, callback) => {
    const url = "http://api.weatherstack.com/current?access_key=39f0c487579ff05abf5b991d78291044&query=" + encodeURIComponent(address)
    request({url, json:true}, (error,{body}) => {
        if(error){
            callback('Unable to connect to forecast service. Check connection and try again¡', undefined)
        } else if (body.error){
            console.log(body.error)
            callback('Did not get location for forecast. Input location again', undefined)
        } else {
            callback(undefined,{
                    location:body.location.country,
                    forecast: "The temperature is " + body.current.temperature + " degrees. And it feels like " + body.current.feelslike + " degrees out."
            
            } )
        }
    })
}

// const forecast = (long, lat, callback) => {
//     const url = "http://api.weatherstack.com/current?access_key=39f0c487579ff05abf5b991d78291044&query=" + long + ',' + lat + "&units=m"
//     request({url: url, json:true}, (error, response) => {
//         if(error){
//             callback('Unable to connect to forecast service. Check connection and try again¡', undefined)
//         } else if (response.body.error){
//             callback('Did not get location for forecast. Input location again', undefined)
//         } else {
//             callback(undefined,{
//                     location:response.body.location.country,
//                     temperature: response.body.current.temperature,
//                     feelslike: response.body.current.feelslike
            
//             } )
//         }
//     })
// }

module.exports = forecast