const request = require('request')


//GEOCODING
//ADDRESS OF THE COUNRTY WHERE WEATHER IS BEING CHECKED FORM --> GEOCODINGAPI(MAPBOX) GETS LATANDLONG ---> SENDS WEATHERCAST API VALUE TO GET WEATHER RESPONSE AND PUSHES BACK TO THE CLIENT

// const mapURL = "https://api.mapbox.com/geocoding/v5/mapbox.places/88kerphjg88.json?access_token=pk.eyJ1IjoiczBsdmlhIiwiYSI6ImNsN2V3d2J0bDA0bmIzcG1vY3B2aWN6bXEifQ.gJv89gq2ZLEydyA6CKmkOg&limit=1"
// request({url: mapURL, json:true}, (error, response) => {
//     if(error) {
//         console.log('Unable to reach mapboot. check internet.')
//     }else if (response.body.features.length === 0) {
//         console.log('No matching results.')
//     } else {
//     const long = response.body.features[0].center[0]
//     const lat = response.body.features[0].center[1]
//    console.log('The longitude of LA is ' + long + " and it's latitude is " + lat)
//     }
// }) 

//CALLBACK AND GEOCODE
const geocode = (address, callback) => {
    const url = "https://api.mapbox.com/geocoding/v5/mapbox.places/ " + encodeURIComponent(address) + ".json?access_token=pk.eyJ1IjoiczBsdmlhIiwiYSI6ImNsN2V3d2J0bDA0bmIzcG1vY3B2aWN6bXEifQ.gJv89gq2ZLEydyA6CKmkOg&limit=1"

    request({url, json: true}, (error, {body}) => {
        if(error) {
            callback('Unable to connect to location service!', undefined)
        } else if (body.features.length === 0){
            callback('Unable to find location. Try another search. ', undefined)
        } else {
            callback(undefined, 
                {
                    longitude :body.features[0].center[0],
                    latitude : body.features[0].center[1],
                    location: body.features[0].place_name
                }
            )
        }
    })
}

module.exports = geocode;