// setTimeout(() => {
//     console.log('2 seconds don reach')
// }, 2000)

// const names = ["Irene", "Ekene","Chinny"]
// const shortnames = names.filter((name) => {
//     return name.length > 5
// })

// console.log(shortnames)

// const geocode = (address, callback) => {
//     setTimeout(() => {
//         const data = {
//             latitude: 0,
//             longitude: 0,
//         }
//         callback(data)
//     }, 2000)
// }

// geocode('Philadelphia', (data) => {
//     console.log(data)
// })

//
// Goal: Mess around with the callback pattern
//
// 1. Define an add function that accepts the correct arguments
// 2. Use setTimeout to simulate a 2 second delay
// 3. After 2 seconds are up, call the callback function with the sum
// 4. Test your work!

const addValue = (a,b, callback) => {
    setTimeout(() => {
        const sum = a + b
        callback(sum)
    },2000)
}

addValue(2,3, (sum) => {
    console.log(sum)
})