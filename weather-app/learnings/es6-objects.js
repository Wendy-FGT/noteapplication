//object property shorthand

const userName = "Chinwendu"
const userAge = 21

const user = {
     userName,
     userAge,
    location: 'Nigeria'
}

console.log(user)

//object destructuring...
//accessing properties from object..extract object properties and their values and store them as single values to be used.

const product = {
    label: "Red notebook",
    price:3,
    stock: 200,
    salePrice: undefined,
    rating: 4.2
}

// const price = product.price
// const stock = product.stock

//destructuring syntax(variable CAN BE RENAMED like the label example. a default value can also be set if the property is not sent like the rating example)
// const {label:productLabel, price, stock, salePrice, rating = 5} = product
// console.log(rating)

//destructuring in a function argument
const transaction = (type, {label, stock = 0} = {}) => {
    console.log(type, label, stock)
}
transaction('order', product)