//ASYNCHRONUS FUNCTION SETIMEOUT

console.log('Starting')

setTimeout(() => {
    console.log('2 second timer')
}, 2000)

setTimeout(() => {
    console.log('Hey there! 0 second timer')
}, 0)

console.log('Stopping')