//REQUEST WEATHER FROM WEATHERCAST API USING REQUEST MODULE/PASSWORD AND CONVERTING TO JSON TO GET JSON RESPONSE
const request = require('request')
const geocode = require('./utils/geocode')
const forecast = require('./utils/forecast')


// const URL = "http://api.weatherstack.com/current?access_key=3f3d2b20753e1b679a80d3f9ed147682&query="

// request({url: URL, json: true},(error, response) => {
//     if (error){
//         console.log('Unable to connect to weather service.')
//     } else if(response.body.error) {
//             console.log('Unable to find location')
//         } else {
//             const data = response.body
//             const temperature = data.current.temperature
//             const precipitation = data.current.feelslike
//             console.log(data.current.weather_descriptions[0] + ". It's currently " + temperature + " degrees out. It feels like " + precipitation + " degrees out.")
//     }
// })

const address = process.argv[2]
if(!address) {
    console.log('Please provide an address!')
}else {
    geocode(address, (error, {location}) => {
        if(error){
            return console.log(error)
        }
        forecast(location, (error, forecastData) => {
            if(error) {
                return console.log(error)
            }
            console.log(location)
            console.log(forecastData)
        })
    })
    
}
