const fs = require('fs')
const jsonObject = require("./1-json.json")

const book = {
    title:'Percy Jackson: Lightning Thief',
    author:'Rick Riodan'
}

// const bookJSON = JSON.stringify(book)
// console.log(bookJSON)

// const parseData = JSON.parse(bookJSON)
// console.log(parseData.author)

//fs.writeFileSync('1-json.json', bookJSON)
//fs.readFileSync('1-json.json')
// const dataBuffer = fs.readFileSync("1-json.json")
// const dataJSON = dataBuffer.toString()
// const data = JSON.parse(dataJSON)
// console.log(data.title)


const dataBuffer = fs.readFileSync('1-json.json')
const dataString = dataBuffer.toString()
const dataJSON = JSON.parse(dataString)

dataJSON.name = "Chinwendu"
dataJSON.age = 22
console.log(dataJSON)
const newData = JSON.stringify(dataJSON)
fs.writeFileSync('1-json.json', newData)