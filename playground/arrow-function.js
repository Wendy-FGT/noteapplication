//NORMAL FUNCTION
//const square = function (x) {
//     return x * x
// }

//STANDARD ARROW FUNCTION
// const square = (x) => {
//     return x * x
// }
// console.log(square(3))

//CONCISE ARROW FUNCTION(FOR SIMPLE FUNCTIONS)
// const square = (x) => x * x
// console.log(square(3))

//ARROW PROPERTIES AS OBJECTS(not well suited for method properties)
const event = {
    name: "Birthday Party",
    guestList: ['Chinwendu', 'Ekene', 'Chinny'],
    printGuestList () {
        console.log('Guest List for ' + this.name)
        
        this.guestList.forEach( (guest) => {
            console.log(guest + ' is attending ' + this.name)
        })
    }
}

event.printGuestList()